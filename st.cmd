require essioc   
require ecmccfg, 7.0.1

epicsEnvSet("ISRC_P", "ISrc-CoolMon") 
iocshLoad("${ecmccfg_DIR}startup.cmd", "IOC=$(ISRC_P),ECMC_VER=7.0.1,EthercatMC_VER=3.0.2,stream_VER=2.8.10")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Configure hardware:
iocshLoad("iocsh/cooling_sensors.iocsh")

#- Load the Database
dbLoadRecords("db/isrc_cooling_sensors.db", "P=$(ISRC_P)")

#- Configure diagnostics:
ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

#- Snippet Load
iocshLoad("$(ecmccfg_DIR)setAppMode.cmd")


